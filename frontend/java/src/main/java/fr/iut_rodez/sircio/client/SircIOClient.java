package fr.iut_rodez.sircio.client;

import io.socket.client.IO;
import io.socket.client.Socket;
import java.net.URI;
import java.util.Scanner;

import fr.iut_rodez.sircio.client.commands.*;

public class SircIOClient {

    public static void printUsageAndExit() {
        System.out.printf("usage: sircio-client <nickname> <server URL>");
        System.exit(1);      
    }
    
    public static void main(String[] args) {

        /* check that the application was given two parameters */
        if (args.length != 2)
            printUsageAndExit();
        
        SircIOParser[] parsers = {
                new SircIOMsgCommand.Parser()
        };

        SircIOConnection connection = new SircIOConnection(args[0], args[1]);

        try {
            
            final Scanner console = new Scanner(System.in);
            
            try {
                
                /* read a line and process it */
                while (console.hasNextLine()) {
                    
                    final String line = console.nextLine();
                    
                    if (line.startsWith("/")) {
                        SircIOCommand cmd = null;
                        for (SircIOParser parser : parsers) {
                            cmd = parser.parse(line);
                            if (cmd != null) {
                                cmd.emitTo(connection.getSocket());
                                break;
                            }
                        }
                        if (cmd == null)
                            System.err.println("unknown command: "+line);
                    } else {
                        connection.sendLine(line);
                    }
                }
                
            } finally {
                console.close();
            }
            
        } finally {
            connection.close();
        }
    }
}
