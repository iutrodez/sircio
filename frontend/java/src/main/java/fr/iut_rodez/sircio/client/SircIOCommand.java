package fr.iut_rodez.sircio.client;

import io.socket.client.Socket;

public interface SircIOCommand {
    void emitTo(Socket socket);
}
