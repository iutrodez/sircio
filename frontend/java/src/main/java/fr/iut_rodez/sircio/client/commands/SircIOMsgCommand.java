package fr.iut_rodez.sircio.client.commands;

import fr.iut_rodez.sircio.client.SircIOCommand;
import fr.iut_rodez.sircio.client.SircIOParser;
import io.socket.client.Socket;

public class SircIOMsgCommand implements SircIOCommand {
    
    public static class Parser implements SircIOParser {
        public SircIOCommand parse(String line) {
            if (!line.startsWith("/msg "))
                return null;
            String[] parts = line.split("\\s+", 3);
            if (parts.length < 3)
                throw new RuntimeException();
            return new SircIOMsgCommand(parts[1], parts[2]);
        }
    }
    
    private String recipient;
    private String message;
    
    public SircIOMsgCommand(String recipient, String message) {
        super();
        // TODO vérifier que les paramètres sont corrects
        this.recipient = recipient;
        this.message = message;
    }
    
    @Override
    public String toString() {
        return "SircIOMsgCommand [recipient=" + this.recipient +
                                 ", message=" + this.message + "]";
    }
    
    public void emitTo(Socket socket) {
        socket.emit("message", this.recipient, this.message);
    }
}
