package fr.iut_rodez.sircio.client;

public interface SircIOParser {
    SircIOCommand parse(String line);
}
