package fr.iut_rodez.sircio.client;

import java.net.URI;

import io.socket.client.IO;
import io.socket.client.Socket;

public class SircIOConnection {
    
    private Socket socket;
    
    public SircIOConnection(String nickname, String serverUrl) {

        // TODO check the format of the two parameters

        /* make a connection to the server */
        final URI uri = URI.create(serverUrl);
        final IO.Options options = IO.Options.builder()/*...*/.build();
        this.socket = IO.socket(uri, options);

        /* listen to connection event, then initialize client with nickname */
        this.socket.on(Socket.EVENT_CONNECT, (Object... objects) -> {
            this.socket.emit("init", nickname);
        });

        /* listen to error messages */
        this.socket.on("error", (Object... objects) -> {
            final String type = (String) objects[0];
            switch (type) {
            case "message":
                System.err.println("unknown user "+objects[1]);
                break;
            default:
                System.err.println("unknown error type: "+type);
            }
        });

        /* listen to notice messages */
        this.socket.on("notice", (Object... objects) -> {
            System.out.printf("%03d %s\n", objects[0], objects[1]);
        });

        /* listen to incoming messages */
        this.socket.on("message", (Object... objects) -> {
            System.out.printf("%s> %s\n", objects[0], objects[1]);
        });

        /* actually connect to the remote server */
        this.socket.open();
    }

    public void close() {
        this.socket.close();
    }
    
    public Socket getSocket() {
        return this.socket;
    }

    public void sendLine(String line) {
        this.socket.emit("line", line);
    }
}
