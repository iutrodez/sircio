```shell
VIRTUAL_ENV=$HOME/.virtualenv

# Prepare isolated environment
virtualenv $VIRTUAL_ENV

# Activate isolated environment
source $VIRTUAL_ENV/bin/activate

# Install package
pip install python-socketio

cd src
uvicorn sircio_server:app
```

https://python-socketio.readthedocs.io
