import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import json from '@rollup/plugin-json';

export default {
    input: 'src/index.js',
    output: {
        file: 'dist/server.mjs',
        format: 'esm'
    },
    plugins: [
        json(),
        commonjs(),
        resolve(),
        process.env.BUILD === 'production' && terser({ mangle: { properties: true }})
    ]
};
