/**
 * Copyright (C) 2020-2021 Johnny Accot <johnny@accot.fr>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

import User from './user';

export default class Users {

    constructor() {
        this.users = new Map();
    }

    addUser(key, nickname, address) {
        console.log(`added user ${nickname} from ${address}`);
        const user = new User(nickname, address, key);
        this.users.set(key, user);
        return user;
    }

    deleteUser(key) {
        const user = this.getUserByKey(key);
        console.log(`removed user ${user.nickname} from ${user.address}`);
        delete this.users[key];
    }

    getUserByKey(key) {
        return this.users.get(key);
    }

    getUserByNickname(nickname) {
        for (let user of this.users.values())
            if (user.nickname === nickname)
                return user;
        return null;
    }
}
