/**
 * Copyright (C) 2020-2021 Johnny Accot <johnny@accot.fr>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

export default class User {

    #nickname;
    #address;
    #key;

    constructor(nickname, address, key) {
        this.#nickname = nickname;
        this.#address = address;
        this.#key = key;
    }

    get nickname() {
        return this.#nickname;
    }

    set nickname(nickname) {
        this.#nickname = nickname;
    }

    get address() {
        return this.#address;
    }

    get key() {
        return this.#key;
    }
}
