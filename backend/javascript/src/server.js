/**
 * Copyright (C) 2020-2021 Johnny Accot <johnny@accot.fr>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

import * as io from 'socket.io';
import Users from './users';

export default class SircioServer {

    constructor(port=8000) {
        this.port = port;
        this.server = new io.Server();
        this.server.on('connection', socket => this.initSocket(socket));
        this.users = new Users();
    }

    initSocket(socket) {

        /* when the user disconnects, delete it from the user list */
        socket.on('disconnect', () => this.users.deleteUser(socket));

        /* handle init command */
        socket.on("init", nickname => {
            const existingUser = this.users.getUserByNickname(nickname);
            if (existingUser)
                socket.emit("error", 'init', nickname);
            else {
                this.users.addUser(socket, nickname, socket.handshake.address);
                socket.emit("notice", 1, `Welcome to Rodez, ${nickname}!`);
            }
        });

        /* handle message command */
        socket.on("message", (recipient, message) => {
            const toUser = this.users.getUserByNickname(recipient);
            if (toUser) {
                const fromUser = this.users.getUserByKey(socket);
                toUser.key.emit("message", fromUser.nickname, message);
            }
            else
                socket.emit("error", 'message', recipient);
        });
    }

    run() {
        this.server.listen(this.port);
    }
}
