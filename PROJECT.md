---
title: Messagerie instantanée
subtitle: Projet tuteuré I.U.T. de Rodez, INFO1S2 2020-2021
author: Sujet proposé par Johnny Accot
date: \today{}
lang: fr-FR
documentclass: scrartcl
header-includes:
  - \hypersetup{colorlinks=false,
            allbordercolors={0 0 0},
            pdfborderstyle={/S/U/W 1}}
...

Ce projet consiste à implémenter un système minimaliste mais fonctionnel de messagerie instantanée. Il pourra intéresser les étudiants qui envisagent des carrières dans des secteurs proposant des services interactifs basés sur les réseaux, tels que les réseaux sociaux, ou les services Web temps-réel en général. Il offre une initiation à la programmation client-serveur, à la conception sur un modèle observateur/observable (*observer pattern*) et au routage.

Le projet est adapté à un groupe de 4 à 6 étudiants (motivés). Comme il contient deux sous-projets,  — la réalisation d'une partie serveur et la réalisation d'une partie client, — 2 ou 3 étudiants pourraient s'intéresser respectivement aux aspects serveur et client.

Enfin, le projet est multi-langage. Les étudiants peuvent choisir d'implémenter le serveur et le client en Java, Javascript ou Python, indépendamment l'un de l'autre. Par exemple, le serveur pourrait être implémenté en Javascript (recommandé) par deux ou trois étudiants qui ont des notions de ce langage ou veulent le découvrir (c'est proche de Java, en beaucoup plus simple), et le client en Java par les autres étudiants. Des configurations minimales utilisables seront fournies aux étudiants pour chacun de ces langages, permettant de lancer un serveur et un client ; il n'est pas nécessaire d'avoir des connaissances réseau particulières.

Noter que, pour pouvoir développer la partie serveur en Java sous Windows, il faudra avoir un accès administrateur à la machine de développement (par exemple votre ordinateur personnel) ; les paramètres de sécurité à l'IUT pourraient ne pas vous permettre de lancer le serveur sur les ordinateurs de l'université. Les étudiants sous Linux n'auront pas ce soucis.

# Présentation

Le but est d'implémenter un système de messagerie instantanée en combinant deux technologies : IRC et `Socket.IO`.

## IRC

L'*internet relay chat*, ou IRC, est un système de messagerie instantanée conçue en 1988 par deux étudiants, Jarkko Oikarinen et Darren Reed. Au cours des années 1990, c'était le moyen privilégié pour les discussions en ligne. Libre de toute influence politique ou commerciale, il reste aujourd'hui le moyen de communication privilégié des développeurs de logiciels libres.

IRC suit un modèle client-serveur. Des serveurs, organisés en réseaux, sont installés dans tous les pays. Des utilisateurs utilisent le client IRC de leur choix pour se connecter à un serveur d'un réseau et discuter avec d'autres utilisateurs de ce réseau. L'interaction se fait nativement au moyen de commandes textuelles, permettant de rejoindre des groupes de discussion, d'écrire à des utilisateurs en privé, ou effectuer des actions d'administration.

IRC est un protocole à part entière au desus de TCP/IP mais il est possible, avec les technologies actuelles, de créer assez facilement une version simplifiée de ce service. C'est ce que propose ce projet, avec l'implémentation d'un serveur et d'un client IRC au dessus de la technologie `Socket.IO`.

## Socket.IO

`Socket.IO` utilise le protocole HTTP pour créer un canal de communication permanent entre un client web (par exemple un navigateur) et un serveur web. Alors qu'HTTP est basé sur un système de requêtes/réponses, `Socket.IO` donne l'impression d'une connexion fixe et bidirectionnelle avec le serveur au moyen de requêtes longues, ou l'utilisation de Websockets. L'avantage est que les aspects réseau sont pratiquement transparents pour le développeur, qui peut se concentrer sur les services qu'il veut offrir.

## Le projet

Le projet consiste à écrire un client et un serveur au dessus de `Socket.IO` pour pouvoir transmettre des commandes IRC entre client et serveur, et permettre la discussion entre utilisateurs.

# Fonctionnement

Un serveur hébergera un ensemble de groupes, nommés salons (*rooms* en anglais). Chaque salon a un nom, de la forme `#nom_du_salon`, et un sujet, c'est-à-dire une courte description de sa fonction. Un utilisateur peut être dans plusieurs salons à la fois, ou dans aucun.

L'utilisateur se connecte au serveur en utilisant un pseudonyme unique, par exemple `pseudo` dans la suite. Au moment de la connexion, le serveur lui envoie un message de bienvenue (`MOTD`, *message of the day*) puis attend des commandes. L'utilisateur va alors envoyer une suite d'instructions que le serveur interprétera. Par le biais de ces commandes, l'utilisateur pourra :

- rejoindre certains salons ;
- envoyer des message à certains utilisateurs précis ;
- lister les groupes existants ;
- inviter des utilisateurs à rejoindre un salon ;
- administrer les salons et leurs utilisateurs ;
- etc. ;
- et, enfin, se déconnecter.

Il est aussi possible de ne pas mettre de commande du tout. Dans ce cas, le texte sera considéré comme un message à destination du salon courant de l'utilisateur.

Par exemple, pour envoyer un message privé à l'utilisatrice `dulcinea`, l'utilisateur `donquixote` saisira dans la console du client la commande (voir l'exemple pour les formats de message) :

```
    /msg dulcinea The words, tell me the words…
```

L'utilisatrice `dulcinea` verra, dans la console de son client, le message :

```
    <donquixote> The words, tell me the words…
```

Elle répondra avec la commande :

```
    /msg donquixote To dream an impossible dream…
```

et `donquixote` verra dans sa console :

```
    <dulcinea> To dream an impossible dream…
```

Les commandes possibles dépendent des implémentations des serveurs et des clients IRC. Nous allons implémenter une sélection de commandes les plus courantes et les plus utiles, qui permettront d'offrir une expérience complète satisfaisante aux utilisateurs.

# Description technique

L'interaction avec le serveur se fera au moyen des commandes décrites ci-dessous. Dans ces commandes, les paramètres de la forme `<param>` sont obligatoires ; ceux de la forme `[param]` sont optionnels.

## Commandes minimales

Les commandes suivantes devront être utilisables par tous les utilisateurs :

- `/help` : Imprime un message d'aide et un résumé des commandes possibles, avec leurs arguments et une courte explication.
- `/list` : Affiche la liste des salons actifs sur le serveur avec la liste des pseudos des utilisateurs dans le salon.
- `/join <#salon>` : Entre dans le salon `<#salon>` si l'on n'y est pas, ou sélectionne le salon comme salon courant si l'on y est déjà. Lorsqu'un utilisateur rejoint un nouveau salon, le sujet du salon est imprimé dans sa console. Le premier utilisateur d'un salon devient l'opérateur de ce salon, c'est-à-dire qu'il a des droits d'administration (voir les commandes des opérateurs ci-dessous).
- `/leave <#salon> [message]` : Quitte le salon `<#salon>`. Si un message est fourni, il est envoyé au salon avant de le quitter. Lorsque le dernier utilisateur a quitté un salon, celui-ci est détruit et n'apparaît plus dans la liste des salons.
- `/msg <pseudo> <message>` : Envoie un message privé <message> à l'utilisateur <pseudo>.
- `/topic <sujet>` : Change le sujet du salon courant à `<sujet>`.
- `/invite <pseudo> <#salon>` : Invite l'utilisateur `<pseudo>` à rejoindre un salon `<#salon>`
- `/away [message]` : L'utilisateur indique au salon qu'il s'abstente temporairement. S'il est fourni, le message est affiché dans le salon avec l'annonce d'absence.
- `/back` (non standard) : L'utilisateur indique qu'il est de retour. Si un utilisateur est `away` et écrit un message dans un salon ou envoie un message privé, il est automatiquement `back`.
- `/quit` : Se déconnecte du serveur.

La gestion des erreurs sera faite naturellement. Par exemple, si un utilisateur demande à quitter un salon dans lequel il n'est pas, ou écrit à un utilisateur inexistant actuellement, il verra un message d'erreur qui l'en informe.

## Commandes des opérateurs

Les opérateurs peuvent effectuer des tâches d'administration sur les salons qu'ils gèrent. Pour cela, ils utilisent la commande `/mode` dont la syntaxe est :

`/mode <#salon> [+|-]<caractère> [paramètre]`

Les caractères et paramètres à implémenter sont :

- `i` : rendre un salon accessible sur invitation uniquement.
- `m` : Rendre le salon modéré. Dans ce cas, seuls les opérateurs peuvent écrire, ou ceux qui ont reçu le privilège avec `+v`.
- `v <pseudo>` : donner à l'utilisateur `<pseudo>`le droit de parole dans un salon modéré.
- `o <pseudo>` : promouvoir `<pseudo>` au rôle d'opérateur du salon.
- `b <pseudo@adresse-IP>` : bannir du salon l'utilisateur `<pseudo>` qui se connecte depuis l'adresse IP `<adresse-IP>`.
- `l <nombre>` : fixer une capacité limitée de `<nombre>` utilisateurs pour le salon.
- `t` : limiter le droit de changer le sujet d'un salon aux opérateurs uniquement.

Par exemple :

- `/mode #usa +o biden` : Fait de `biden` un opérateur de `#usa`.
- `/mode #usa -o trump` : Retire les droits d'opérateurs sur `#usa` à `trump`.
- `/mode #politesse +b troll@1.2.3.4` : Bannit du salon `#politesse` l'utilisateur `troll` connecté depuis l'adresse `1.2.3.4`.
- `/mode #politesse -b troll@1.2.3.4` : Annule le bannissement précédent.
- `/mode #jardinsecret +i` : Rend le salon `#jardinsecret` joignable sur invitation uniquement.

Si l'utilisateur ne mentionne pas de signe, `+` ou `-`, par défaut ce sera un `+`.

## Commandes optionnelles

Une fois que les commandes précédentes auront été implémentées, on pourra ajouter certaines des commandes suivantes :

- `/whois <pseudo>` : Imprime les informations sur l'utilisateur `<pseudo>`.
- `/me <message>` : Affiche un message d' « action » dans le salon. Par exemple, si votre pseudo est `tityre`, la commande `/me sourit.` imprimera le message « * tityre sourit. » dans le salon.
- `/nick <nouveau_pseudo>` : Change de pseudo.
- `/notify <pseudo>` : Permet de savoir quand un utilisateur se connecte sur le serveur.
- `/ignore <pseudo>` : Ignore un utilisateur, c'est-à-dire que vous ne voyez plus ce qu'il écrit. Pour désactiver ce filtrage, on utilisera `/unignore <pseudo>`.
- `/op <pseudo>` : Alias de la commande `/mode <#salon> +o <pseudo>` dans le <#salon>, servant à donner les droits d'opérateurs à un utilisateur du salon

# Travail à réaliser

Il faudra :

- établir un cahier des charges précis ;
- réaliser un diagramme de cas d'utilisation UML ;
- faire une description écrite des cas d’utilisation ;
- réaliser la conception de l'application en utilisant notamment un diagramme de classes UML ;
- développer la partie serveur en Java, Javascript ou Python, au choix, et la partie client en Java ou Javascript ;
- développer des programmes de tests unitaires les plus complets possible, à l'exclusion des tests liés au réseau (difficiles à réaliser) ;
- réaliser des tests d'intégration.

Le code initial client-serveur et la définition du projet (ce document) sont disponibles à l'URL [https://codeberg.org/iutrodez/sircio](https://codeberg.org/iutrodez/sircio). Pour le récupérer sur sa machine automatiquement, on pourra exécuter la commande suivante dans une invite de commande Windows ou un terminal Linux (après avoir installé [`git`](https://git-scm.com/downloads)) :

```shell
    git clone https://codeberg.org/iutrodez/sircio.git
```

## Documents à rendre

À l'issue du projet, un dossier technique devra être rendu. Il comportera :

- le cahier des charges, les documents d'analyse, les spécifications détaillées ;
- un document de conception intégrant le diagramme de classes et des explications pour les éléments les plus complexes ;
- la répartition des tâches au sein de l'équipe ;
- la liste des objectifs atteints et non atteints ;
- un dossier de tests comportant ;
    - le code source des programmes écrits pour réaliser les tests unitaires ;
    - le résultat de l'exécution de ces programmes : des copies d'écran et des explications ;
    - les scénarios de tests (avec les résultats) effectués pour tester globalement l'application ;
- des exemples de code source bien écrit et commenté, avec des explications rédigées en plus des commentaires présents dans le code. Il ne faut pas insérer dans le dossier la totalité du code source, mais seulement une vingtaine de pages. Il vous appartient de sélectionner le code source le plus pertinent ;
- un manuel utilisateur ;
- un bilan.

La qualité de la rédaction (clarté, précision, mise en forme, orthographe) sera prise en compte dans l'évaluation du projet, ainsi que la rédaction éventuelle des commentaires et l'utilisation de variables en anglais.

## Documents numériques à déposer sur le serveur

- tout le code source du projet, y compris les programmes de test ;
- un exécutable du jeu ;
- un fichier au format pdf correspondant au dossier décrit dans la section précédente.

# Exemple concret

Vous trouverez ci-dessous un exemple réel (mais reconstitué) avec un serveur IRC en utilisant l'utilitaire `tinyirc` sous Linux (avec des lignes omises et des modifications). Vous pouvez vous inspirer de ces données pour formater vos messages, mais vous êtes libres de proposer vos propres formats. Il ne serait pas inutile d'essayer de vous connecter vous-mêmes à un serveur IRC pour avoir une idée de l'interaction.

**`$ tinyirc accot irc.freenode.net`**

```
TinyIRC 1.1 Copyright (C) 1991-1996 Nathan Laredo
This is free software with ABSOLUTELY NO WARRANTY.
For details please see the file COPYING.
IRCNAME: accot IRCLOGIN: accot
*** Real user is Johnny Accot
*** trying port 6667 of irc.freenode.net
-cherryh.freenode.net- *** Looking up your hostname...
-cherryh.freenode.net- *** Found your hostname
001 Welcome to the freenode Internet Relay Chat Network accot
002 Your host is cherryh.freenode.net[158.69.67.207/6667], running version ircd-1.1.9
003 This server was created Wed Oct 28 2020 at 22:01:42 UTC
254 46223 channels formed
255 I have 4573 clients and 1 servers
375 - cherryh.freenode.net Message of the Day -
372 - Welcome to cherryh.freenode.net in Montreal, CA.
372 - Welcome to freenode - supporting the free and open source
372 - software communities since 1998.
372 - Thank you for using freenode!
376 End of /MOTD command.
```

**`/whois accot`**

```
= WHOIS accot
*** accot is ~accot@###.proxad.net * accot
319 accot +#test
312 accot leguin.freenode.net Umeå, SE, EU
378 accot is connecting from *@###.proxad.net ###.###.###.###
*** accot idle 1630 second(s), on since Mon Mar 15 19:20:36 2021
318 accot End of /WHOIS list.
```

**`/nick johnny`**

```
= NICK johnny
*** accot is now known as johnny
```


**`/join #aveyron`**

```
= JOIN #aveyron
*** Now talking in #aveyron
*** cherryh.freenode.net changed #aveyron to: +ns
353 @ #aveyron @johnny
366 #aveyron End of /NAMES list.
324 #aveyron +ns
*** #aveyron formed Tue Mar 16 08:09:35 2021
```

**`/topic #aveyron Où il fait bon vivre`**

```
= TOPIC #aveyron Où il fait bon vivre
*** johnny set #aveyron topic to "Où il fait bon vivre"
```

**`/join #iut-rodez`**

```
= JOIN #iut-rodez
*** Now talking in #iut-rodez
*** cherryh.freenode.net changed #iut-rodez to: +ns
353 @ #iut-rodez @johnny
366 #iut-rodez End of /NAMES list.
324 #iut-rodez +ns
*** #iut-rodez formed Tue Mar 16 08:27:19 2021
```

**`/topic #iut-rodez Le temps des projets`**

```
= TOPIC #iut-rodez Le temps des projets
*** johnny set #iut-rodez topic to "Le temps des projets"
```

```
= LIST #iut-rodez
321 Channel Users  Name
322 #aveyron 1 Où il fait bon vivre
322 #iut-rodez 1 Le temps des projets
323 End of /LIST
```

Un étudiant rejoint le groupe #iut-rodez ; on écrit un message, qui ira dans le salon courant, soit `#iut-rodez` ; nous recevons une réponse d'un étudiant ; celui-ci quitte le salon ; un utilisateur `aveyronnais` rejoint le salon `#aveyron` et envoie un message (noter le format différent pour les messages provenant des salons non courants) :

**`Bonjour à l'étudiant.`**

```
*** etudiant (~etudiant@###.proxad.net) joined #iut-rodez
> Bonjour à l'étudiant.
<etudiant> Bonjour à l'enseignant.
*** etudiant (~etudiant@###.proxad.net) left #iut-rodez
*** aveyronnais (~aveyronna@###.proxad.net) joined #aveyron
<aveyronnais:#aveyron> Salut l'Aveyron !
```

On envoie un message à l'étudiant en privé et celui-ci nous répond :

**`/msg etudiant Ça va en TD ?`**

```
-> *etudiant* Ça va en TD ?
<- *etudiant* Bien sûr !
```

**`/away pause café`**

```
= AWAY pause café
306 You have been marked as being away
```

On quitte le salon `#iut-rodez` ; le salon courant devient `#aveyron` :

**`/leave #iut-rodez`**

```
= LEAVE #iut-rodez
*** johnny (~johnny@###.proxad.net) left #iut-rodez
*** Now talking in #aveyron
```

**`/quit`**

```
= QUIT :TinyIRC 1.1
*** johnny (~johnny@###.proxad.net) Quit (Quit: TinyIRC 1.1)
 Closing Link: ###.proxad.net (Quit: TinyIRC 1.1)
```

# Liens utiles sur wikipedia

- [IRC](https://fr.wikipedia.org/wiki/Internet_Relay_Chat)
- [Socket.IO](https://en.wikipedia.org/wiki/Socket.IO) (en anglais)
- [Observer pattern](https://fr.wikipedia.org/wiki/Observateur_(patron_de_conception))
